// global variables
const submit = document.querySelector('#submit'); // submit
const capture = document.querySelector('#capture'); // capture buttom
const data = document.querySelector('#data'); // stats location
const pokeCaptured = document.querySelector('#pokeCaptured'); // pokemons captured
let imgStorage = "";
let pokeArray = [];

// Functions
function dataApi() { // function to call the API
    const pkmn = document.querySelector('#pkmn').value.toLowerCase(); // input value
    fetch(`https://pokeapi.co/api/v2/pokemon/${pkmn}/`)
        .then(res => res.json()) // translates to json
        .then(async res => {
            const img = res.sprites.front_default;
            const name = res.name;
            const id = res.id;
            const type = res.types.map(e => e.type.name).join(", ");
            const abilities = res.abilities.map(e => e.ability.url);
            const language = await fetchAbilities(abilities);  // calling the endpoint
            const location = await fecthLocations(id);
            const pokeData = {img, name, id, type, language, location}; 
            printPokemon(pokeData); // calling the result with added tags
            imgStorage = img;
        })
        .catch(() => {
            capture.innerHTML = "";
            data.innerHTML = "";
            printError(); // calling the result with added tags 
        })    
}

const fetchAbilities = async abilities => {
    const fetched = abilities.map(url => fetch(url).then(res => res.json())) // array with all fetched abilities
    const res = await Promise.all(fetched) // wait the responses
    return res.map(res => {
        const abyLanguage = res.names.find(e => e.language.name === "en");
        const en = abyLanguage.name;
        return en;
    }).join(", ")
}

async function fecthLocations(id) { // function to call the endpoint
    const res = await fetch(`https://pokeapi.co/api/v2/location/${id}/`);
    const array = await res.json();
    const areaLanguage = array.names.find(e => e.language.name === "en");
    const area = areaLanguage.name;
    return area;
}

function printPokemon(pokeData) { // result found
    capture.innerHTML = `<input type="submit" value="Capture" class="btn btn-dark mt-3">`;
    data.innerHTML = `
     <img class="sprite" src="${pokeData.img}"/>
     <h6>Name: <b class="text-success">${pokeData.name}</b>, Nº${pokeData.id}</h6>
     <hr>
     <h6>Type: <b class="text-info">${pokeData.type}</b></h6>
     <hr>
     <h6>Abilities: <b class="text-danger">${pokeData.language}</b></h6>
     <hr>
     <h6>Location: <b class="text-warning">${pokeData.location}</b></h6>
    `;
}

function printError() { // result not found
    data.innerHTML = "";
    data.innerHTML = `<img class="error" src="img/404.jpg"/>`;
}

function createItem(imgStorage) {
    let item = {img: imgStorage};
    pokeArray.push(item);
    return item;
}

function savePokemon() { // save pokemon in localstorage
    if (pokeArray.length < 7) {
        localStorage.setItem('pokemon', JSON.stringify(pokeArray)); 
        loadPokemon();
    }
}

function loadPokemon() {
    pokeCaptured.innerHTML = "";
    pokeArray = JSON.parse(localStorage.getItem('pokemon'));

    if(pokeArray === null){
        pokeArray = [];
    } else {
        pokeArray.forEach(e => {
            pokeCaptured.innerHTML +=  `<label><img src="${e.img}"><i class="fas fa-trash"></i></label>`;
        });
    }
}

function deletePokemon(img) {
    let indexArray;
    pokeArray.forEach((element, index) => {
        if (element.img === img) {
            indexArray = index;
        }
    });
    pokeArray.splice(indexArray,1);
    savePokemon();
}

// Events
submit.addEventListener('click',function(e){
    e.preventDefault();
    dataApi(); // calling and printPokemon the API result
}) 

capture.addEventListener('click',function(e){
    e.preventDefault();
    createItem(imgStorage)
    savePokemon();
})

document.addEventListener('DOMContentLoaded', loadPokemon);

pokeCaptured.addEventListener('click',function(e){
    e.preventDefault();
    if(e.path[0].nodeName === 'path'){
        let element = e.path[2].childNodes[0].currentSrc;
        deletePokemon(element);
    }
})